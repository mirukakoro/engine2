from direct.showbase import ShowBaseGlobal
from panda3d.bullet import BulletWorld, BulletPlaneShape, BulletRigidBodyNode
from panda3d.core import PointLight, Spotlight, PerspectiveLens

from assets import Assets
from ball import Ball

import gltf  # add glTF support for Panda3D

from plate import PlateOld, Plate
from showbase import ShowBase

print(gltf)  # trick linting to think we use it

base = ShowBase()
assets = Assets.from_path('./config.toml')

world = BulletWorld()
world.set_gravity((0, 0, -9.87))

shape = BulletPlaneShape((0, 0, 1), 1)
node = BulletRigidBodyNode('Ground')
node.addShape(shape)
np = base.render.attachNewNode(node)
np.setPos(0, 0, -50)

world.attachRigidBody(node)


def update(task):
    dt = ShowBaseGlobal.globalClock.getDt()
    world.doPhysics(dt)
    return task.cont


base.task_mgr.add(update, 'update')

light = PointLight('Base Light')
light.set_color((10, 10, 10, 10))
lightnp = base.render.attachNewNode(light)
lightnp.set_hpr(0, -90, 0)
lightnp.set_pos(0, 0, 10)
base.render.setLight(lightnp)

for x in range(3):
    for y in range(3):
        plate = Plate(assets)
        plate.apply(base, world)
        plate.np.set_pos((x-1) * 30, 0, (y-1) * 30)
        plate.node.set_mass(1.0)
        ball = Ball(assets)
        ball.apply(base, world)
        ball.np.set_pos((x-1) * 30, 0, (y-1) * 30 + 5)
        ball.orig_pos = ((x-1) * 30, 0, (y-1) * 30 + 5)
        ball.physics_node.set_mass(1.0)

slight = Spotlight('Spotlight')
slight.set_color((1000, 1000, 1000, 1))
lens = PerspectiveLens()
slight.set_lens(lens)
slightnp = base.render.attach_new_node(slight)
base.render.set_light(slightnp)


@base.task('Track Ball')
def slightnp_track(task):
    slightnp.set_hpr(base.camera.get_hpr())
    slightnp.set_pos(base.camera.get_pos())
    return task.cont


@base.task('Toggle Spotlight')
def toggle_spotlight(task):
    if round(task.time, 1) % 1.0 == 0.0:
        slight.set_color((0, 0, 0, 0))
    return task.cont


if __name__ == '__main__':
    base.run()
