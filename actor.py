from typing import Dict, Callable

from direct.actor.Actor import Actor as Actor_
from panda3d.bullet import BulletTriangleMesh, BulletTriangleMeshShape, BulletWorld, BulletRigidBodyNode, BulletShape, \
    BulletBodyNode


class Actor(Actor_):
    def __init__(
            self,
            model_path: str,
            anim_paths: Dict[str, str] = None,
            shape: BulletShape = None,
            node: BulletBodyNode = None,
            pre_apply: Callable = None,
            post_apply: Callable = None,
    ):
        if anim_paths is None:
            anim_paths = {}
        Actor_.__init__(
            self,
            model_path,
            anim_paths,
        )

        self.pre_apply = pre_apply,
        self.post_apply = post_apply
        self.np = None
        self.shape = shape
        if shape is None:
            mesh = BulletTriangleMesh()
            mesh.add_geom(
                self.getGeomNode().findAllMatches('**/+GeomNode').getPath(0).node().getGeom(0),
                # get all nodes of the actor and use it as the physics mesh thing
            )
            self.shape = BulletTriangleMeshShape(mesh, dynamic=True)

        self.node = node
        if node is None:
            self.node = BulletRigidBodyNode('Plate')
            self.node.add_shape(self.shape)

    def apply(self, base, world: BulletWorld):
        if isinstance(self.pre_apply, Callable):
            self.pre_apply(base, world)

        self.np = base.render.attach_new_node(self.node)
        self.reparent_to(self.np)
        world.attach_rigid_body(self.node)

        if isinstance(self.post_apply, Callable):
            self.post_apply(base, world)

    def rm(self):
        self.cleanup()
