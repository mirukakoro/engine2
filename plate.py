from direct.actor.Actor import Actor
from panda3d.bullet import BulletTriangleMesh, BulletTriangleMeshShape, BulletWorld, BulletRigidBodyNode

from actor import Actor as ActorNew
from assets import Assets


class Plate(ActorNew):
    def __init__(
            self,
            assets: Assets,
    ):
        ActorNew.__init__(
            self,
            assets['plate']['path'],
            {},
            pre_apply=self.__pre_apply,
            post_apply=self.__post_apply,
        )

    def __pre_apply(self, base, world):
        self.node.set_mass(1.0)

    def __post_apply(self, base, world):
        base.task_mgr.add(self.__auto_right),

    def __auto_right(self, task):
        self.node.set_angular_velocity(tuple(map(lambda x: -(x / 100), self.np.get_hpr())))
        return task.cont


class PlateOld(Actor):
    def __init__(
            self,
            assets: Assets,
    ):
        Actor.__init__(
            self,
            assets['plate']['path'],
            {},
        )
        mesh = BulletTriangleMesh()
        mesh.add_geom(
            self.getGeomNode().findAllMatches('**/+GeomNode').getPath(0).node().getGeom(0),
            # get all nodes of the actor and use it as the physics mesh thing
        )
        self.shape = BulletTriangleMeshShape(mesh, dynamic=True)
        self.node = BulletRigidBodyNode('Plate')
        self.node.add_shape(self.shape)
        self.node.set_mass(1.0)
        self.np = None

    def apply(self, base, world: BulletWorld):
        self.np = base.render.attach_new_node(self.node)
        self.reparent_to(self.np)
        base.task_mgr.add(self.__auto_right)
        world.attach_rigid_body(self.node)

    def __auto_right(self, task):
        self.node.set_angular_velocity(tuple(map(lambda x: -(x / 100), self.np.get_hpr())))
        return task.cont

    def rm(self):
        self.cleanup()
