from showbase import ShowBase


class Plugin:
    def __init__(self, showbase: ShowBase):
        self.showbase = showbase

    @staticmethod
    def plugin(showbase: ShowBase):
        return Plugin(showbase)
